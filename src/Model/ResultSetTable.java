/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.swing.table.AbstractTableModel;
import java.sql.SQLException;

/**
 *
 * @author moacirnq
 */
public class ResultSetTable extends AbstractTableModel {
    private Connector con;
    private int numberOfRows;
    
    //verifica o estado da conexão
    private boolean connectedToDatabase = false;
    
    //Construtor inicializa o resultSet
    public ResultSetTable(String query ){
        
        con = new Connector();
        
        try
        {
            con.connect();
            connectedToDatabase = true;
            fireTableStructureChanged();
        }
        catch(Exception e)
        {
            connectedToDatabase = false;
            e.printStackTrace();
        }
       try{
            setQuery(query);
       }catch (Exception e){
           e.printStackTrace(); 
       }
    }
    
    //
    public Class getColumnClass( int column ) {
        //se assegura que esta conectado
        if(!con.status)
            throw new IllegalStateException( "Not connected to database");
        
        //determine Java class of column
        try 
        {
            String className = con.resultMetadata().getColumnClassName( column + 1);
            return Class.forName( className );
        }
        catch ( Exception exception )
        {
            exception.printStackTrace();
        }
        
        return Object.class;
    }
    
    //get Number of columns
    public int getColumnCount() throws IllegalStateException
    {
        if (!con.status)
           throw new IllegalStateException("Not connected to database");
        
        try
        {
            return con.resultMetadata().getColumnCount();
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
        
        return 0;
    }
    
    //get name of a particular column in resultset
    public String getColumnName(int column) throws IllegalStateException
    {
        if (!con.status)
            throw new IllegalStateException("Not connected to database");
        
        try
        {
            return con.resultMetadata().getColumnName(column + 1);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return "";
    }
    
    public int getRowCount() throws IllegalStateException
    {
        if (!con.status)
            throw new IllegalStateException("Not connected to database");
        
        return numberOfRows; 
    }
    
    public Object getValueAt( int row, int column) throws IllegalStateException
    {
        if (!con.status)
            throw new IllegalStateException("Not connected to database");
        
        try 
        {
            con.result().absolute(row+1);
            return con.result().getObject(column+1);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return "";
    }
    
    public Object getValueAt( int row, String column) throws IllegalStateException
    {
        if (!con.status)
            throw new IllegalStateException("Not connected to database");
        
        try 
        {
            con.result().absolute(row+1);
            return con.result().getString(column);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return "";
    }
    
    
    public void setQuery( String query ) throws SQLException, IllegalStateException
    {
        if (!con.status)
            throw new IllegalStateException("Not connected to database");
        
        con.query(query);
        
        con.result().last();
        numberOfRows = con.result().getRow();
        
        fireTableStructureChanged();
    }
    
    
    public void disconnectFromDatabase()
    {
        try 
        {
            con.disconnect();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}