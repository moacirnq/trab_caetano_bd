/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Guilherme
 */
public class Vendedor {
    private String codigo;
    private String senha;
    private String pnome;
    private String nmeio;
    private String sobrenome;
      
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    
    public void setSenha(String senha){
        this.senha = senha;
    }
    
    public void setNome(String pnome, String nmeio, String sobrenome){
        this.pnome = pnome;
        this.nmeio = nmeio;
        this.sobrenome = sobrenome;
    }
    
    public String getCodigo(){
        return this.codigo;
    }
    
    public String getSenha(){
        return this.senha;
    }
    
    public String getPnome(){
        return this.pnome;
    }
    
    public String getNmeio(){
        return this.nmeio;
    }
    
    public String getSnome(){
        return this.sobrenome;
    }

    private static Vendedor instance = null;
    private Vendedor(){    
    }
    
    public static Vendedor getInstance(){
        if(instance == null) {
            instance = new Vendedor();
        }
        return instance;
    }
}
