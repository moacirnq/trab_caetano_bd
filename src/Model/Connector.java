/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 *
 * @author moacirnq
 */
public final class Connector {
    //database URL
    static final String DATABASE_URL = "jdbc:oracle:thin:@grad.icmc.usp.br:15214:orcl14";
    private static String Username = "gt11";
    private static String Password = "gt11";
    
    private  Connection connection = null;	//Manages connection
    private  Statement statement = null;	//query statements
    private  ResultSet resultSet = null;	//manages results
    private  ResultSetMetaData metaData = null;
    public   boolean status = false;
          
    public void connect() throws SQLException
    {
        if (status) return;
        //stabilish connection to database
        connection = DriverManager.getConnection(DATABASE_URL, Username, Password);

        //create Statement for querying database
        statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        status = true;        
    }
    
    public void query(String q) throws SQLException{
        resultSet = statement.executeQuery(q);
        
        //process query resutls
        metaData = resultSet.getMetaData();
    }
    
    public void update(String q) throws SQLException{
        statement.executeUpdate(q);
    }
    
    public  ResultSetMetaData resultMetadata(){
        return metaData;
    }
    
    public ResultSet result(){
        return resultSet;
    }
    
    public  void disconnect() {
        try {
            status = false;
            if (resultSet != null) resultSet.close();
            statement.close();
            connection.close();
	}
        catch (Exception e) {
            e.printStackTrace();
	}
    }
    
    public Connection getConnection() {
        return connection;
    }
}