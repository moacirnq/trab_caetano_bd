/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author moacirnq
 */
public class ComboItem {
    private String text;
    private String value;
    private float value2;

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }
    
    public ComboItem(String text, String value) {
        this.value = value;
        this.text = text;
    }
    
    public ComboItem(String text, String value, float value2) {
        this.value = value;
        this.text = text;
        this.value2 = value2;
    }

    public float getValue2() {
        return value2;
    }
    
    @Override
    public String toString() {
        return text;
    }
}
