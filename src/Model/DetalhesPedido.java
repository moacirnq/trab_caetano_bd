/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author moacirnq
 */
public class DetalhesPedido {
    private int codPedido;

    public int getCodPedido() {
        return codPedido;
    }

    public void setCodPedido(int codPedido) {
        this.codPedido = codPedido;
    }

    public String getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public float getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(float precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public float getDesconto() {
        return desconto;
    }

    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }
    private String codProduto;
    private int qtd;
    private String nomeProduto;
    private float precoUnitario;
    private float desconto;
    
    public DetalhesPedido( int codPedido, String codProduto, int qtd, float preco,
                            float desconto) {
        this.codPedido = codPedido;
        this.codProduto = codProduto;
        this.qtd = qtd;
        this.precoUnitario = preco;
        this.desconto = desconto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }
    
    public DetalhesPedido( int codPedido, String codProduto, String nomeProduto, int qtd, float preco,
                            float desconto) {
        this.codPedido = codPedido;
        this.codProduto = codProduto;
        this.nomeProduto = nomeProduto;
        this.qtd = qtd;
        this.precoUnitario = preco;
        this.desconto = desconto;
    }
}
