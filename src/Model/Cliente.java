/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author moacirnq
 */
public class Cliente {
    private int codigo;
    private String tratamento;
    private String primeiroNome;
    private String nomeDoMeio;
    private String sobrenome;
    private String sufixo;
    private String senha;
    
    public Cliente(int codigo, String tratamento, String primeiroNome, String nomeDoMeio, 
            String sobrenome, String sufixo, String senha) {
        this.codigo = codigo;
        this.tratamento = tratamento;
        this.primeiroNome = primeiroNome;
        this.nomeDoMeio = nomeDoMeio;
        this.sobrenome = sobrenome;
        this.sufixo = sufixo;
        this.senha = senha;
    }

    public String getTratamento() {
        return tratamento;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getPrimeiroNome() {
        return primeiroNome;
    }

    public String getNomeDoMeio() {
        return nomeDoMeio;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public String getSufixo() {
        return sufixo;
    }

    public String getSenha() {
        return senha;
    }
    
    
}
