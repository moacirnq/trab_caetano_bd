/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.File;
import java.io.IOException;
/**
 *
 * @author guilherme
 */
public class newPDF extends File{
    public newPDF(String name){
        super(name+".PDF");
        
        
        if(this.createNewFile())
            System.out.println("Relatório criado com sucesso.");
    }
    
    public boolean createNewFile(){
        try{
            super.createNewFile();
            return true;
        }catch(IOException e){
            System.out.println("Erro ao gerar relatório.");
            e.printStackTrace();
            return false;
        }
    }
}
