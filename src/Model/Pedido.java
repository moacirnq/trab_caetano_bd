/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.sql.SQLException;

/**
 *
 * @author moacirnq
 */
public class Pedido {

    //variable definition
    private int codigo;
    private String dataPedido;
    private String dataEnvio;
    private String dataRecebimento;
    private int codigoCliente;
    private String conta;
    private String numeroCartao;
    private String codigoConfirmacao;
    private int codigoVendedor;
    private float imposto;
    private int codigoEndFat;
    private int codigoEndRec;
    private int codigoTransportadora;
    //end of variable definition

    //Construtor
    public Pedido(int codigo,String dtped, String dtenvio, String dtrec,
            int cliente, String conta, String cartao, String conf,
            int vendedor, float imposto, int endfat, int endrec, int transp) {
        setCodigo(codigo);
        setDataEnvio(dtenvio);
        setDataPedido(dtped);
        setDataRecebimento(dtrec);
        setCodigoCliente(cliente);
        setConta(conta);
        setNumeroCartao(cartao);
        setCodigoConfirmacao(conf);
        setCodigoVendedor(vendedor);
        setImposto(imposto);
        setCodigoEndFat(endfat);
        setCodigoEndRec(endrec);
        setCodigoTransportadora(transp);
    }
    
    //getters and setters
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(String dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public String getDataRecebimento() {
        return dataRecebimento;
    }

    public void setDataRecebimento(String dataRecebimento) {
        this.dataRecebimento = dataRecebimento;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public String getCodigoConfirmacao() {
        return codigoConfirmacao;
    }

    public void setCodigoConfirmacao(String codigoConfirmacao) {
        this.codigoConfirmacao = codigoConfirmacao;
    }

    public int getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(int codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public float getImposto() {
        return imposto;
    }

    public void setImposto(float imposto) {
        this.imposto = imposto;
    }

    public int getCodigoEndFat() {
        return codigoEndFat;
    }

    public void setCodigoEndFat(int codigoEndFat) {
        this.codigoEndFat = codigoEndFat;
    }

    public int getCodigoEndRec() {
        return codigoEndRec;
    }

    public void setCodigoEndRec(int codigoEndRec) {
        this.codigoEndRec = codigoEndRec;
    }

    public int getCodigoTransportadora() {
        return codigoTransportadora;
    }

    public void setCodigoTransportadora(int codigoTransportadora) {
        this.codigoTransportadora = codigoTransportadora;
    }
}
