/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPCell;
import javax.swing.JTable;

/**
 *
 * @author guilherme
 */
public class writer {
    public Document doc = new Document();
    
    public void addMetaData(String titulo, String assunto, String keywords, String autor, String criador){
        this.doc.addTitle(titulo);
        this.doc.addSubject(assunto);
        this.doc.addCreator(criador);
        this.doc.addAuthor(autor);
        this.doc.addKeywords(keywords);
    }
    
    public Paragraph newParagraph(String text, boolean center, boolean left, boolean right){
        Paragraph para = new Paragraph();
        if((center) && (!left) && (!right)){
            para.setAlignment(Element.ALIGN_CENTER);
        }
        else if((!center) && (left) && (!right)){
            para.setAlignment(Element.ALIGN_LEFT);
        }
        else if((!center) && (!left) && (right)){
            para.setAlignment(Element.ALIGN_RIGHT);
        }
        else System.out.println("Alinhamento incorreto.");
        para.add(text);
        return para;
    }
    
    /*
    addTable - adiciona a tabela no arquivo pdf
    argumentos de entrada:
        -String para - título da tabela;
        -int col - número de colunas;
        -String[][] rowData - dados da tabela;
        -JTable origina - JTable que contém os dados.
    */
    public Section addTable(String para, int nCol, String[][] rowData, JTable original){
        Section tableSection = new Chapter(0).addSection(this.newParagraph(para, true, false, false));
        
        tableSection.add(new Paragraph(" "));
        PdfPTable table = new PdfPTable(nCol);
        
        for(int i = 0; i < nCol; i++)
        {
            table.addCell(new PdfPCell(new Phrase(original.getModel().getColumnName(i).toString())));
        }
        
        for(String[] data: rowData){
            for (String element: data){
                table.addCell(element);
            }
        }
        
        tableSection.add(table);
        
        return tableSection;
    }
    
    public void newPage(){
        this.doc.newPage();
    }
    
    public void addToPage(Element add) throws DocumentException{
        this.doc.add(add);
    }
    
}
