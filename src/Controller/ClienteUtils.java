/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ComboItem;
import Model.Connector;
import Model.Cliente;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author moacirnq
 */
public class ClienteUtils {
    public static String getNome(int codigo) {
        String text = "";
        ComboItem aux;
        Connector con = new Connector();
        try {
            con.connect();
            con.query("select codigo, nome "
                    + "from view_nomecliente where codigo = " + codigo);
            while (con.result().next()) {
                text = con.result().getString(2);
                con.disconnect();
                return text;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        con.disconnect();
        return text;
    }
    
    public static ComboItem getCodigoFromNome(String nome, String meio, String sobrenome) {
        Connector con = new Connector();
        try {
            con.connect();
            con.query("select codigo, primeironome || ' ' || nomedomeio || ' ' || sobrenome "
                    + "as nome from cliente where nome like '" + nome + "%'" );
            if (con.result().next())
                return new ComboItem(con.result().getString(2), 
                                     con.result().getString(1));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static DefaultComboBoxModel<ComboItem> nameComboModel() {
        return UtilStuff.comboModel("select codigo, nome from view_nomecliente order by nome");
    }
    
    public static ComboItem getClosest(String nome) {
        ComboItem ret = null;
        Connector con = new Connector();
        
        try {
            con.connect();
            con.query("SELECT CODIGO, NOME FROM VIEW_NOMECLIENTE WHERE NOME LIKE '"+nome+"%'");
            while(con.result().next()) {
                ret =  new ComboItem(con.result().getString("nome"),con.result().getString("codigo"));
                con.disconnect();
                return ret;
            }
        }
        catch (SQLException e){
            con.disconnect();
        }
        return ret;
    }
    
    /* Gera um novo codigo para um cliente. Se nao conseguir gerar, retorna -1**/
    public static int getNewCode() {
        int codigo = -1;
        Connector con = new Connector();
        
        try {
            con.connect();
            con.query("SELECT SEQ_CODIGO_CLIENTE.NEXTVAL FROM DUAL");
            con.result().next();
            codigo = con.result().getInt(1);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            con.disconnect();
        }
        return codigo;
    }
    
    public static void insert(Cliente cliente) throws SQLException {
        Connector con = new Connector();
        String query;
        
        query = "INSERT INTO CLIENTE VALUES (" +
                cliente.getCodigo() + ", '" +
                cliente.getTratamento()+ "', '" +
                cliente.getPrimeiroNome() + "', '" +
                cliente.getNomeDoMeio()+ "', '" +
                cliente.getSobrenome()+ "', '" +
                cliente.getSufixo()+ "', '" +
                cliente.getSenha()+ "')";
        
        con.connect();
        con.update(query);
        con.disconnect();
    }
}
