/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Connector;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import Model.Vendedor;
import View.*;
/**
 *
 * @author Guilherme
 */
public class LabBD {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        loginVendedor tela_login = new loginVendedor();
        tela_login.setVisible(true);
         
    }
    
    static public void showNovoPedido(){
        FormPedidoInserir telaNovoPedido = new FormPedidoInserir();
        telaNovoPedido.setVisible(true);
    }
    
    static public void showConsultaVendedor(){
     ConsultaVendedor tela_consulta = new ConsultaVendedor();
     tela_consulta.setVisible(true);
    }
    
    static public void showTopClientes(){
     RelatorioTopClientes tela_top = new RelatorioTopClientes();
     tela_top.setVisible(true);
    }
    
    static public void showRelatorio1(){
        DatasmartRegiaoTempo tela_regiaoTempo = new DatasmartRegiaoTempo();
        tela_regiaoTempo.setVisible(true);
    }
    static public void showRelatorio2(){
        FormRelatorio2 rel2 = new FormRelatorio2();
        rel2.setVisible(true);
    }
    static public void showRelatorio3(){
        RelatorioProdutoTempo rel3 = new RelatorioProdutoTempo();
        rel3.setVisible(true);
    }
    
}

