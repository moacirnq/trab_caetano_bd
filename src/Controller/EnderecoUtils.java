/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ComboItem;
import Model.Connector;
import java.sql.SQLException;
import javax.swing.JComboBox;
import java.sql.*;
import oracle.jdbc.*;

/**
 *
 * @author moacirnq
 */
public class EnderecoUtils {
    /** Preenche uma combobox com enderecos **/
    public static void fillClienteComboBox(JComboBox cmbBox, int codigo) {
        String text;
        String value;
        Connector con = new Connector();
        cmbBox.removeAllItems();
        try {
            con.connect();
            con.query("select e.id, e.logradouro || ', ' || e.complemento || ', ' || e.cidade || ' - ' ||"
                    + " e.estado || ' - ' || e.pais || '-CEP: ' || e.codigopostal  as end "
                    + " from endereco e, clienteendereco ce where ce.idenredeco=e.id and ce.codigocliente=" + codigo +
                    "order by end");
            while (con.result().next()) {
                text = con.result().getString(2);
                value = con.result().getString(1);
                ComboItem ci = new ComboItem(text, value);
                cmbBox.addItem(ci);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static int getItemClienteFromCode(int codEnd, int codCli, JComboBox cmb) {
        String text;
        String value="";
        ComboItem aux;
        Connector con = new Connector();
        try {
            con.connect();
            con.query("select e.id, e.logradouro || ', ' || e.complemento || ', ' || e.cidade || ' - ' ||"
                    + "e.estado || ' - ' || e.pais || '-CEP: ' || e.codigopostal "
                    + "from endereco e, clienteendereco ce where ce.IDENREDECO=e.id and ce.codigocliente=" 
                    + codCli + " and e.id=" +codEnd);

            while (con.result().next()) {
                text = con.result().getString(2);
                value = con.result().getString(1);
                break;
            }
            for (int i =0 ; i < cmb.getModel().getSize(); i++) {
                aux = (ComboItem) cmb.getModel().getElementAt(i);
                if (aux.getValue() == value)
                    return i;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static void insertEnderecoCliente(int codCliente, String logradouro,
            String complemento, String cidade, String estado, String pais, 
            String codPostal, String tipo) throws SQLException{
        
        Connector con = new Connector();
        con.connect();
        CallableStatement proc = con.getConnection().prepareCall(
            "{ call CADASTRAR_ENDERECO_CLIENTE(?, ?, ?, ?, ?, ?, ?, ?)}");
        proc.setInt(1, codCliente);
        proc.setString(2, logradouro);
        proc.setString(3, complemento);
        proc.setString(4, cidade);
        proc.setString(5, estado);
        proc.setString(6, pais);
        proc.setString(7, codPostal);
        proc.setString(8, tipo);
        proc.execute();
        con.getConnection().commit();
        con.disconnect();
    }
}
