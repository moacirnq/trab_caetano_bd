/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.util.HashMap;

/**
 *
 * @author guilherme
 */
public class Relatorios {
    
    public static String getPath(){
        return newPDF.separator;
    }
    //seleciona os clientes com mais de 15 compras
    public ResultSetTable getTopClientes(){
        String query = "select case when NOMEDOMEIO is null or NOMEDOMEIO = null then "+
                        "c.PRIMEIRONOME ||' '||c.SOBRENOME " +
                        "else (PRIMEIRONOME ||' '||NOMEDOMEIO||' '||SOBRENOME) end as Nome_Completo, pedidos.num_pedidos from " +
                        " (select C.CODIGO, count(*) as num_pedidos from cliente C, pedido P " +
                        "where C.CODIGO = P.CODIGOCLIENTE " +
                        "group by C.CODIGO) pedidos, cliente C " +
                        "where pedidos.num_pedidos > 15 and C.CODIGO = pedidos.CODIGO " +
                        "order by num_pedidos desc";
        ResultSetTable rs = new ResultSetTable(query);
        return rs;
    }
    //seleciona os vendedores que cumpriram sua quota de vendas em um período de tempo
    public ResultSetTable getQuota(String dataInicio, String dataFim){
        
        String query =  "select case when v.nomedomeio is not null then\n" +
                        "(v.PRIMEIRONOME ||' '||v.NOMEDOMEIO||' '||v.SOBRENOME)\n" +
                        "else (v.PRIMEIRONOME ||' '||v.SOBRENOME) end as Nome_Completo, v.quota,\n" +
                        "SUM(dp.precounitario * dp.quantidade - dp.precounitario * dp.quantidade *dp.desconto) as Vendas\n" +
                        "from pedido pe, vendedor v, detalhespedido dp\n" +
                        "where v.codigo = pe.codigovendedor and dp.codigopedido = pe.codigo\n" +
                        "and pe.dtpedido > '"+ dataInicio + "'\n" +
                        "and pe.dtpedido < '" + dataFim + "'\n" +
                        "group by (v.primeironome, v.nomedomeio, v.sobrenome, v.quota) having\n" +
                        "SUM(dp.precounitario * dp.quantidade - dp.precounitario * dp.quantidade *dp.desconto) > v.quota\n" +
                        "order by vendas desc";
        ResultSetTable rs = new ResultSetTable(query);
        return rs;
    }
    
    //gera o relatório de clientes em PDF
    public void printClientes(JTable clientes) throws DocumentException, FileNotFoundException{
        
        Model.writer writer = new Model.writer();
        
        
        PdfWriter.getInstance(writer.doc, new FileOutputStream(new newPDF ("Relatorio_Clientes")));
        writer.doc.open();
        writer.addToPage(writer.newParagraph("Melhores clientes da loja", true, false, false));
        //writer.addToPage(new Paragraph(" "));
        writer.addToPage(writer.addTable("Clientes com mais de 15 compras", 
                clientes.getModel().getColumnCount(), this.getTableData(clientes), clientes));
        writer.newPage();
        writer.doc.close();
        
    }
    
    //gera o relatório de vendedores em PDF
    public void printQuota(JTable clientes, String dataInicio, String dataFim) throws DocumentException, FileNotFoundException{
        
        Model.writer writer = new Model.writer();
        
        
        PdfWriter.getInstance(writer.doc, new FileOutputStream(new newPDF ("Relatorio_Quotas")));
        writer.doc.open();
        
        writer.addToPage(writer.newParagraph("Vendedores que cumpriram a quota", true, false, false));
        //writer.addToPage(new Paragraph(" "));
        writer.addToPage(writer.addTable("Pesquisa entre "+ dataInicio + " e " + dataFim+":\n", 
                clientes.getModel().getColumnCount(), this.getTableData(clientes), clientes));
        writer.newPage();
        writer.doc.close();
        
    }
    
    //retorna os dados da tabela para sua escrita no PDF
    public static String [][] getTableData(JTable table){
        int nLin = table.getModel().getRowCount();
        int nCol = table.getModel().getColumnCount();
        
        String [][] tableData = new String[nLin][nCol];
        
        for(int i = 0; i < nLin; i++)
            for(int j = 0; j < nCol; j++)
                if (table.getValueAt(i, j) !=null)
                    tableData[i][j] = (String)table.getValueAt(i, j).toString();
                else
                    tableData[i][j] = "";
        return tableData;
    }
    
    
    //faz a análise Região x Produto
    public static DefaultTableModel relVendaRegiaoProduto(String dataInicio, String dataFim, String regiao, String prodGran){
        
        DefaultTableModel model = null;
        try {
            String query;
            Connector con = new Connector();
            HashMap<String, HashMap<String, String>> matriz = new HashMap<String, HashMap<String, String>>();
            Vector <String> cols = new Vector<String>();
            int numberOfCollumns;
            con.connect();
            
            
            //Monta colunas
            cols.add("Product");
            con.query("SELECT DISTINCT "+ regiao +" AS NOME FROM DMREGIAO");
            while (con.result().next()) {
                String col = con.result().getString("NOME");
                cols.add(col);
            }
            
            
            if (prodGran == "Unitario") {
                con.query("SELECT DISTINCT NOME FROM DMPRODUTO");
                while (con.result().next()) {
                    String col = con.result().getString("NOME");
                    matriz.put(col, new HashMap<String,String>());
                }
            }
            else {
                    matriz.put("Total", new HashMap<String,String>());
            }
            
            if (prodGran=="Unitario") 
                query = "SELECT * FROM (SELECT P.NOME AS PRODUTO, R."+regiao+" AS REGIAO, SUM(V.QUANTIDADE*V.PRECOUNITARIO) AS TOTAL FROM FTVENDA V\n" +
"                        JOIN DMPRODUTO P ON P.CODIGO = V.CODIGOPRODUTO \n" +
"                        JOIN DMREGIAO R ON R.CODIGO = V.CODIGOREGIAO \n" +
"                        JOIN DMTEMPO T ON T.CODIGO = V.CODIGOTEMPO\n" +
"                        WHERE T.DATA >= TO_DATE('"+dataInicio+"','dd/MM/YYYY') AND T.DATA <= TO_DATE('"+dataFim+"','dd/MM/YYYY')\n" +
"                        GROUP BY CUBE (P.NOME, R."+regiao+")) WHERE \n" +
"                        PRODUTO IS NOT NULL AND REGIAO IS NOT NULL";
            
            else
                query = "SELECT * FROM (SELECT P.NOME AS PRODUTO, R."+regiao+" AS REGIAO, SUM(V.QUANTIDADE*V.PRECOUNITARIO) AS TOTAL FROM FTVENDA V\n" +
"                        JOIN DMPRODUTO P ON P.CODIGO = V.CODIGOPRODUTO \n" +
"                        JOIN DMREGIAO R ON R.CODIGO = V.CODIGOREGIAO \n" +
"                        JOIN DMTEMPO T ON T.CODIGO = V.CODIGOTEMPO\n" +
"                        WHERE T.DATA >= TO_DATE('"+dataInicio+"','dd/MM/YYYY') AND T.DATA <= TO_DATE('"+dataFim+"','dd/MM/YYYY')\n" +
"                        GROUP BY CUBE (P.NOME, R."+regiao+")) WHERE \n" +
"                        PRODUTO IS NULL AND REGIAO IS NOT NULL";
            
            con.query(query);
            
            while (con.result().next()) {
                if (prodGran == "Unitario") 
                {
                    String produto = con.result().getString("PRODUTO");
                    String reg = con.result().getString("REGIAO");
                    String total = con.result().getString("TOTAL");
                    matriz.get(produto).put(reg, total);
                }
                else {
                    String total = con.result().getString("TOTAL");
                    String reg = con.result().getString("REGIAO");
                    matriz.get("Total").put(reg, total);
                }
            }
            
            model = new DefaultTableModel(null,cols);
            
            for (String key : matriz.keySet()) {
                int i = 0;
                String[] row = new String[cols.size()+1];
                row[0] = key;
                for(String col : cols) {
                    if (i!=0)
                        row[i] = matriz.get(key).get(col);
                    i++;
                }
                model.addRow(row);
            }
            
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return model;
    }
    
    //gera os resultados de Região x Produto em PDF
    public static void printRelVendaProduto(JTable tbl, String dataInicio, String dataFim) throws DocumentException, FileNotFoundException {
        Model.writer writer = new Model.writer();
        
        
        PdfWriter.getInstance(writer.doc, new FileOutputStream(new newPDF ("Relatorio2")));
        writer.doc.open();
        
        writer.addToPage(writer.newParagraph("Relatorio de vendas x produtos x regiao", true, false, false));
        //writer.addToPage(new Paragraph(" "));
        writer.addToPage(writer.addTable("Pesquisa entre "+ dataInicio + " e " + dataFim+":\n", 
                tbl.getModel().getColumnCount(), getTableData(tbl), tbl));
        writer.newPage();
        writer.doc.close();
    }
    
    //gera os resultados de Tempo x Região em PDF
    public static void printRelTempoRegiao(JTable tbl, String dataInicio, String dataFim) throws DocumentException, FileNotFoundException{
        
        Model.writer writer = new Model.writer();
        
        
        PdfWriter.getInstance(writer.doc, new FileOutputStream(new newPDF ("Relatorio_TempoRegiao")));
        writer.doc.open();
        
        writer.addToPage(writer.newParagraph("Analise de Tempo x Regiao", true, false, false));
        //writer.addToPage(new Paragraph(" "));
        writer.addToPage(writer.addTable("Analise de Tempo x Região entre "+ dataInicio+" e "+ dataFim +": \n", 
                tbl.getModel().getColumnCount(), getTableData(tbl), tbl));
        writer.newPage();
        writer.doc.close();
        
    }
    
    //gera os resultados de Produto x Tempo em PDF
    public static void printRelProdutoTempo(JTable jt, String dataInicio, String dataFim) throws DocumentException, FileNotFoundException{

        Model.writer writer = new Model.writer();

        PdfWriter.getInstance(writer.doc, new FileOutputStream(new newPDF ("Relatorio_ProdXTempo")));
        writer.doc.open();

        writer.addToPage(writer.newParagraph("Análise de Produto x Tempo", true, false, false));

        writer.addToPage(writer.addTable("Pesquisa entre "+ dataInicio+ " e "+dataFim+": \n",
            jt.getModel().getColumnCount(), getTableData(jt), jt));

        writer.newPage();
        writer.doc.close();
    }
}
