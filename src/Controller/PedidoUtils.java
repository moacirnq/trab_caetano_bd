/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Vector;
import javax.swing.JTable;

/**
 *
 * @author moacirnq
 */
public class PedidoUtils {
    /**Checa se o codigo corresponde a uma classe existente. Se existir retorna
    um objeto com os dados. Senao retorna nulo.**/
    public static Pedido exists(String codigo) {
        Connector con = new Connector();
        Pedido pedido = null;
        
        try {
            int code = Integer.parseInt(codigo);
            con.connect();
            con.query("SELECT * FROM PEDIDO WHERE CODIGO=" + codigo + "");
            while (con.result().next()) {
                pedido = new Pedido(
                        con.result().getInt("codigo"),
                        con.result().getString("dtpedido"),
                        con.result().getString("dtenvio"),
                        con.result().getString("dtrecebimento"),
                        con.result().getInt("codigocliente"),
                        con.result().getString("contacliente"),
                        con.result().getString("numerocartaocredito"),
                        con.result().getString("codigoconfirmacao"),
                        con.result().getInt("codigovendedor"),
                        con.result().getFloat("imposto"),
                        con.result().getInt("enderecofatura"),
                        con.result().getInt("enderecoentrega"),
                        con.result().getInt("codigotransportadora")
                        );
            }
            con.disconnect();
            return pedido;
        }
        catch (NumberFormatException e) {
            return null;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**Recupera uma transacao. Senao encontrar retorna nulo.**/
    public static Pedido retrieve(String codigo, Vector<DetalhesPedido> dets) {
        Connector con = new Connector();
        Pedido pedido = null;
        
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            int code = Integer.parseInt(codigo);
            con.connect();
            con.query("SELECT  P.CODIGO, P.DTPEDIDO, P.DTENVIO, P.DTRECEBIMENTO, "
                    + "P.CODIGOCLIENTE, P.DTRECEBIMENTO, P.CONTACLIENTE, "
                    + "P. NUMEROCARTAOCREDITO, P.CODIGOCONFIRMACAO, P.CODIGOVENDEDOR, "
                    + "P.IMPOSTO, P.ENDERECOFATURA, P.ENDERECOENTREGA, P.CODIGOTRANSPORTADORA, "
                    + "D.CODIGOPEDIDO, D.CODIGOPRODUTO, D.QUANTIDADE, D.PRECOUNITARIO, "
                    + "D.DESCONTO, PR.NOME AS NOMEPRODUTO "
                    + "FROM PEDIDO P, DETALHESPEDIDO D, PRODUTO PR WHERE "
                    + "P.CODIGO=CODIGOPEDIDO AND P.CODIGO=" + codigo + " AND PR.CODIGO = D.CODIGOPRODUTO");
            while (con.result().next()) {
                pedido = new Pedido(
                        con.result().getInt("codigo"),
                        dateFormat.format(con.result().getDate("dtpedido")),
                        dateFormat.format(con.result().getDate("dtenvio")),
                        dateFormat.format(con.result().getDate("dtrecebimento")),
                        con.result().getInt("codigocliente"),
                        con.result().getString("contacliente"),
                        con.result().getString("numerocartaocredito"),
                        con.result().getString("codigoconfirmacao"),
                        con.result().getInt("codigovendedor"),
                        con.result().getFloat("imposto"),
                        con.result().getInt("enderecofatura"),
                        con.result().getInt("enderecoentrega"),
                        con.result().getInt("codigotransportadora")
                        );
                dets.add(
                        new DetalhesPedido(
                            con.result().getInt("codigopedido"),
                            con.result().getString("codigoproduto"),
                            con.result().getString("nomeproduto"),
                            con.result().getInt("quantidade"),
                            con.result().getFloat("precounitario"),
                            con.result().getInt("desconto")
                        )
                );
            }
            con.disconnect();
            return pedido;
        }
        catch (NumberFormatException e) {
            return null;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /** Gera um codigo para um novo Pedido **/
    public static int newCode() {
        int codigo=-1;
        
        //Gera um novo codigo
        Connector con = new Connector();
        try {
            con.connect();
            con.query("SELECT SEQUENCE_CODIGO_PEDIDO.NEXTVAL FROM DUAL");
            con.result().next();
            codigo = con.result().getInt(1);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        
        return codigo;
    }
    
    /** Insere um pedido no  banco. **/
    private static void insertPedido(Pedido ped, Connector con) throws SQLException {
        String query = "Insert into PEDIDO values ('" +
                ped.getCodigo() + "', TO_DATE('" +
                ped.getDataPedido()+ "', 'dd/mm/yyyy'), TO_DATE('" +
                ped.getDataEnvio()+ "', 'dd/mm/yyyy'), TO_DATE('" +
                ped.getDataRecebimento()+ "', 'dd/mm/yyyy'), " +
                ped.getCodigoCliente()+ ", '" +
                ped.getConta()+ "', '" +
                ped.getNumeroCartao()+ "', '" +
                ped.getCodigoConfirmacao()+ "', " +
                ped.getCodigoVendedor()+ ", " +
                ped.getImposto()+ ", " +
                ped.getCodigoEndFat()+ ", " +
                ped.getCodigoEndRec()+ ", " +
                ped.getCodigoTransportadora()+ ")" ;
        con.update(query);
    }
    
    /** Insere um detalhe de pedido no  banco. **/
    private static void insertDetalhe(DetalhesPedido det, Connector con) 
        throws SQLException {
        
        String query = "Insert into DETALHESPEDIDO values (" +
                det.getCodPedido() + ", '" +
                det.getCodProduto() + "', " +
                det.getQtd()+ ", " +
                det.getPrecoUnitario()+ ", " +
                det.getDesconto()+ ")";
        con.update(query);

    }
    
    /** Atualiza um pedido no  banco. **/
    public static void updatePedido(Pedido ped, Connector con) throws SQLException {

        con.update("update PEDIDO set " +
                "DTPEDIDO=TO_DATE('"+ped.getDataPedido()+ "', 'dd/mm/yyyy'), " +
                "DTENVIO=TO_DATE('"+ped.getDataEnvio()+ "', 'dd/mm/yyyy'), " +
                "DTRECEBIMENTO=TO_DATE('"+ped.getDataRecebimento()+ "', 'dd/mm/yyyy'), " +
                "CODIGOCLIENTE="+ped.getCodigoCliente()+ ", " +
                "CONTACLIENTE='"+ped.getConta()+ "', " +
                "NUMEROCARTAOCREDITO='"+ped.getNumeroCartao()+ "', " +
                "CODIGOCONFIRMACAO='"+ped.getCodigoConfirmacao()+ "', " +
                "CODIGOVENDEDOR="+ped.getCodigoVendedor()+ ", " +
                "IMPOSTO="+ped.getImposto()+ ", " +
                "ENDERECOFATURA="+ped.getCodigoEndFat()+ ", " +
                "ENDERECOENTREGA="+ped.getCodigoEndRec()+ ", " +
                "CODIGOTRANSPORTADORA="+ped.getCodigoTransportadora()+ "WHERE " +
                "CODIGO="+ ped.getCodigo() + ""
        );
    }
    
    public static void insertTransaction( Pedido pedido, Vector<DetalhesPedido> det) throws SQLException {
        //Desabilita o auto commit para simular uma transacao

        Connector con = new Connector();
        con.connect();
        con.getConnection().setAutoCommit(false);

        //Insere o pedido
        insertPedido(pedido, con);

        //Insere os detalhes
        for (DetalhesPedido d : det) {
            insertDetalhe(d, con);
        }
        con.getConnection().commit();
        con.disconnect();
    }
    
    public static void updateTransaction( Pedido pedido, Vector<DetalhesPedido> det) throws SQLException {
        //Desabilita o auto commit para simular uma transacao

        Connector con = new Connector();
        con.connect();
        con.getConnection().setAutoCommit(false);

        //Insere o pedido
        updatePedido(pedido, con);

        //Apaga os detalhes
        con.update("DELETE FROM DETALHESPEDIDO WHERE CODIGOPEDIDO="+pedido.getCodigo());
        
        //Insere os detalhes
        for (DetalhesPedido d : det) {
            insertDetalhe(d, con);
        }
        con.getConnection().commit();
        con.disconnect();
    }
}
