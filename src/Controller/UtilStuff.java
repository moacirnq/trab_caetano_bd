/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.Vector;
import Model.ComboItem;
import Model.Connector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.text.ParseException;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author koiti
 */
public final class UtilStuff {
    //funcao auxiliar de query para evitar duplicação de codigo
     public static ResultSet executeQuery(String query){
        ResultSet rs;
        Connector con = new Connector(); 
        try {
            con.connect();
            con.query(query);
        }catch(SQLException e){
            e.printStackTrace();
        }
            
        rs = con.result();
        return rs;
    }
    
    //traca formato de timestamp da base para DD/MM/yyyy
    public static String FormatData(String data){
        DateFormat originalFormat = new SimpleDateFormat ("yyyy-MM-DD hh:mm:ss");
        DateFormat myFormat = new SimpleDateFormat("DD/MM/yyyy");
        String myDate = "";
        try {
            myDate = myFormat.format(originalFormat.parse(data));
        
        }catch (Exception e){
            e.printStackTrace();
        }
        return myDate;
    }
    
    
   //Le endereco da tabela e formata resultado em String 
   public static String getEndereco(String codEndereco){
       String query = "select * from endereco where id ="+ codEndereco;
       ResultSet rs = UtilStuff.executeQuery(query);
       String lograd = "";
       String comp = "";
       String cidade = "";
       String estado = "";
       String pais = "";
       String codPost = "";
       String resp ="";
       try{
            rs.next();
            lograd = rs.getString("logradouro");
            comp = rs.getString("complemento");
            cidade = rs.getString("cidade");
            estado = rs.getString("estado");
            pais = rs.getString("pais");
            codPost = rs.getString ("codigopostal");
       }catch(SQLException ex){
            ex.printStackTrace();
       } 
       
       if ((comp == null)||(comp.equals("null"))){
           
           resp = lograd+" - " +cidade+" - "+estado +" - "+pais+" - "+codPost; 
       }
       else{
           resp = lograd+" - " +comp+" - "+cidade+" - "+estado+" - "+pais +" - "+ codPost;
       }
       return resp;
   }
   
   /** Preenche uma combobox com os resultados de uma query. As colunas da query
    devem ser do tipo "inteiro, texto". **/
    public static void fillComboBox(JComboBox cmbBox, String query) {
        String text;
        String value;
        Connector con = new Connector();
        cmbBox.removeAllItems();
        try {
            con.connect();
            con.query(query);
            while (con.result().next()) {
                text = con.result().getString(2);
                value = con.result().getString(1);
                ComboItem ci = new ComboItem(text, value);
                cmbBox.addItem(ci);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /** Retorna um DefaultComboBoxModel<ComboItem> com os items da query. **/
    public static DefaultComboBoxModel<ComboItem> comboModel(String query) {
        int numberOfRows = 0;
        Connector con;
        DefaultComboBoxModel<ComboItem> model;
        Vector<ComboItem> results;
        ComboItem aux;
        
        try {
            con = new Connector();
            con.connect();
            con.query(query);
            con.result().last();
            numberOfRows = con.result().getRow();
            con.result().first();
            
            results = new Vector<ComboItem>(numberOfRows);
            do {
                aux = new ComboItem(con.result().getString(2), con.result().getString(1));
                results.add(aux);
            } while (con.result().next());
            model = new DefaultComboBoxModel<ComboItem>(results);
        }
        catch (SQLException e) {
            e.printStackTrace();
            model = null;
        }
        return model;
    }
    
    /** Retorna um ComboItem com o par codigo, texto especificado por uma query.**/
    public static ComboItem getItemFromCode(String query) {
        String text;
        String value;
        Connector con = new Connector();
        try {
            con.connect();
            con.query(query);
            while (con.result().next()) {
                text = con.result().getString(2);
                value = con.result().getString(1);
                return new ComboItem(text, value);
            }
            con.disconnect();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
   public static String getNomeFormated (String nome){
       int i;
       String primeironome="";
       String nomedomeio="";
       String sobrenome="";
       
       String whereCond;
       String split[]= nome.split(" ");
       primeironome = split[0];
       sobrenome = split[split.length-1];
       whereCond = "primeironome = '"+primeironome+"' and sobrenome ='"+sobrenome+"'";
       //nome do meio not null
       System.out.println(split.length);
       if (split.length>2){
           nomedomeio = split[1];
           if (split.length>3){
              for(i=2;i<split.length-1;i++)
                  nomedomeio = nomedomeio.concat(" ").concat(split[i]);
           }
           whereCond = whereCond.concat(" and nomedomeio = '"+nomedomeio+"'");
       }
       return whereCond;
   }
   
   /** Verifica se o campo tem um int valido. Caso seja invalido, 
    * seta o foco e muda a cor do elemento.
    * @param txt
    * @return 
    */
   public static Boolean verifyInt(JTextField txt) {
       try {
            Integer.parseInt(txt.getText());
            txt.setBackground(Color.white);
            return true;
        }
        catch( NumberFormatException e) {
            txt.setBackground(Color.red);
            txt.requestFocusInWindow();
            return false;
        }
   }
   
   /** Verifica se o campo tem um int valido. Caso seja invalido, 
    * seta o foco e muda a cor do elemento.
    * @param txt
    * @return 
    */
   public static Boolean verifyFloat(JTextField txt) {
       try {
            Float.parseFloat(txt.getText());
            txt.setBackground(Color.white);
            return true;
        }
        catch( NumberFormatException e) {
            txt.setBackground(Color.red);
            txt.requestFocusInWindow();
            return false;
        }
   }
   
   public static Boolean verifyCombo(JComboBox cmb) {
       if (cmb.getSelectedItem()==null) {
           cmb.requestFocusInWindow();
           cmb.setBackground(Color.red);
           return false;
       }
       else {
           cmb.setBackground(Color.white);
           return true;
       }
   }
   
   /** Verifica se o campo tem um int valido. Caso seja invalido, 
    * seta o foco e muda a cor do elemento.
    * @param txt
    * @return 
    */
   public static Boolean verifyDate(JTextField txt) {
        if (txt.getText() == "") return true;
        try {
            DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
            df.setLenient(false);
            df.parse(txt.getText());
            txt.setBackground(Color.white);
            return true;
        } 
        catch (ParseException e) {
            txt.setBackground(Color.red);
            txt.requestFocusInWindow();
            return false;
        }
   }
   
   public static Boolean openPDFFile (String path){
         try {

		File pdfFile = new File(path);
		if (pdfFile.exists()) {
                    if (Desktop.isDesktopSupported()) {
                        Desktop.getDesktop().open(pdfFile);
                    } 
                    else {
                        System.out.println("Awt Desktop is not supported!");
                        return false;
                    }

		} else {
                    System.out.println("File is not exists!");
                    return false;
		}
                
		System.out.println("Done");
                return true;
	  } catch (Exception ex) {
		ex.printStackTrace();
	  }
         return false;
   }
}

