/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author koiti
 */
public class ConsultaVendedorController { 
    public final static String CONSULTA_NOME = "case when nomedomeio ='NULL' or nomedomeio is NULL then primeironome||' '||sobrenome else primeironome||' '||nomedomeio||' '||sobrenome end as nome";    
    
    
    public String[] getCodigoVendedores() {
        ArrayList<String> codigos = new ArrayList<String>();
        String arrayCodigos[];
        ResultSet rs;
        String query = "select codigo from Vendedor";
        rs = UtilStuff.executeQuery(query);
        try{
        while (rs.next()){
            codigos.add(rs.getString("codigo"));
        }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        if (codigos.size()>0){
            arrayCodigos = codigos.toArray(new String[codigos.size()]);
        }
        else{
            arrayCodigos = new String[1];            
            arrayCodigos[0] = "empty";
        }
        
        return arrayCodigos;   
    }
    
    public String[] getNomeVendedores(){
        ArrayList<String> nomes = new ArrayList<String>();
        String arrayNomes[];
        ResultSet rs;
        String query = "select "+CONSULTA_NOME+" from vendedor";
        rs = UtilStuff.executeQuery(query);
        try{
        while (rs.next()){
            nomes.add(rs.getString("nome"));
        }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        if (nomes.size()>0){
            arrayNomes = nomes.toArray(new String[nomes.size()]);
        }
        else{
            arrayNomes = new String[1];            
            arrayNomes[0] = "empty";
        }
        return arrayNomes;
    }
    
    public String getNomeById(String id){
        String query ="select nome from view_NomeVendedor where codigo = "+id;
        ResultSet rs = UtilStuff.executeQuery(query);
        String nome = "None";
        try{
            rs.next();
            nome = rs.getString("nome");
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return nome;
    }
    
    public String getCodigoByName(String name){
        String query = "select codigo from view_NomeVendedor where nome = '" +name+"'";
        ResultSet rs = UtilStuff.executeQuery(query);
        
        String cod="";
        try{
            rs.next();
            cod = rs.getString("codigo");  
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return cod;
    }
    
    //public String getIdByName(String Name){
        
   // }
    
    public ResultSetTable pesquisaCompleta (String extention){
        ResultSetTable rs;
        String query = "select " +
                       "p.codigo as codigopedido,c.codigo as codigocliente, " +
                       "p.dtpedido, " +
                       "case when c.nomedomeio ='NULL' or c.nomedomeio is NULL then c.primeironome||' '||c.sobrenome else c.primeironome||' '||c.nomedomeio||' '||c.sobrenome end as nomecliente, " +
                       "p.codigovendedor, " +
                       "case when v.nomedomeio ='NULL' or v.nomedomeio is NULL then v.primeironome||' '||v.sobrenome else v.primeironome||' '||v.nomedomeio||' '||v.sobrenome end as nomevendedor, " +
                       "vp.total "+
                       "from " +
                       "pedido p, cliente c,vendedor v, view_valores_pedidos vp " +
                       "where " +
                       "p.codigocliente = c.codigo and " +
                       "v.codigo = p.codigovendedor and "+
                       "vp.codigo = p.codigo " + extention;  
        
        rs = new ResultSetTable(query);
        return rs;
    }
    
    public ResultSetTable getClientes(){
        String query = "select * from view_nomecliente";
        ResultSetTable rs = new ResultSetTable (query);
        return rs;
    }
    
    public ResultSetTable pesquisaPedidosOnline (String extention){
        ResultSetTable rs;
        String query = "select p.codigo as codigopedido,c.codigo as codigocliente, "
                + "p.dtpedido, case when c.nomedomeio ='NULL' or c.nomedomeio is NULL then c.primeironome||' '||c.sobrenome else c.primeironome||' '||c.nomedomeio||' '||c.sobrenome end as nomecliente, "
                + "vp.total from pedido p, cliente c, view_valores_pedidos vp where p.codigocliente = c.codigo and p.codigovendedor is NULL "
                + "and vp.codigo = p.codigo " + extention;
        System.out.println(query);
        rs = new ResultSetTable(query);
        return rs;
    }
    
    public ResultSetTable getPedidoByCode(String codPedido){
        ResultSetTable rs = new ResultSetTable(
        "select p.codigo as codigopedido, p.dtpedido, p.codigocliente, case when c.nomedomeio ='NULL' or c.nomedomeio is NULL then "+
        "c.primeironome||' '||c.sobrenome else c.primeironome||' '||c.nomedomeio||' '||c.sobrenome end "+
        "as nomecliente , vp.total from pedido p, cliente c, view_valores_pedidos vp where p.codigocliente = c.codigo and p.codigo = vp.codigo and p.codigo = '"+codPedido+"'");
        return rs;
    }
    
    public ResultSetTable pesquisaPorId(String id){
        ResultSetTable rs;
        String query = "select p.codigo as codigopedido,p.dtpedido,c.codigo as codigocliente,"
                + "case when c.nomedomeio ='NULL' or c.nomedomeio is NULL then "
                + "c.primeironome||' '||c.sobrenome else c.primeironome||' '||c.nomedomeio||' '||c.sobrenome end "
                + "as nomecliente , vp.total "
                + "from view_valores_pedidos vp, pedido p, cliente c "
                + "where vp.codigo = p.codigo and p.codigocliente = c.codigo and p.codigovendedor = "+ id;
        rs = new ResultSetTable (query);
        return rs;
    }  
    
    public ResultSetTable pesquisaPorId(String id, String extensao){
        ResultSetTable rs;
        String query = "select p.codigo as codigopedido,p.dtpedido,c.codigo as codigocliente,"
                + "case when c.nomedomeio ='NULL' or c.nomedomeio is NULL then "
                + "c.primeironome||' '||c.sobrenome else c.primeironome||' '||c.nomedomeio||' '||c.sobrenome end "
                + "as nomecliente , vp.total "
                + "from view_valores_pedidos vp, pedido p, cliente c "
                + "where vp.codigo = p.codigo and p.codigocliente = c.codigo and p.codigovendedor = "+ id 
                + extensao;
         
        rs = new ResultSetTable (query);
        return rs;
    }
    
}
