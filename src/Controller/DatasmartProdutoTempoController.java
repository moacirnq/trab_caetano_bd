package Controller;

import Model.newPDF;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DatasmartProdutoTempoController {
    String query;

    public ResultSet getProdutoTempo(String tempo, String dataInicio, String dataFinal){
        int i;
        ResultSet rs;
        String cols = "pr.nome, tmp."+tempo+" ";
        
        //seleciona a soma das vendas do produto em um determinado período de tempo
        if(tempo.equals("Ano"))
            query = "select "+ cols + ", sum(v.quantidade * v.precounitario) as total ";
        else
            query = "select "+ cols + ", tmp.ano, sum(v.quantidade * v.precounitario) as total ";
        query += "from ftVenda v inner join dmProduto pr on (pr.codigo = v.codigoproduto) "+
        "inner join dmTempo tmp on (tmp.codigo = v.codigotempo) "+
        "where tmp.data >= '" + dataInicio + "' and tmp.data <= '"+ dataFinal + "' ";
        if(tempo.equals("Ano"))
            query += "group by (" + cols + ") order by pr.nome, ano";
        else
            query += "group by (" + cols + ", ano) order by pr.nome, ano, "+tempo;
        rs = UtilStuff.executeQuery(query);
        return rs;
    }

    //organiza os resultados de getProdutoTempo na tabela
    public Colunas getColumn(String tempo){
        Colunas colunas;
        String colQuery;
        String cmp = "";
        String ano = "";
        if(tempo.equals("Ano")){
            colQuery = "select distinct ano from ("+ query + ") order by ano";
            colunas = new Colunas(1, tempo, "produto");
        }
        else{
            colQuery = "select distinct "+ tempo +", ano from ("+ query + ") order by ano, "+tempo+" ";
            colunas = new Colunas(2, tempo, "produto"); 
        }
        ResultSet rs = UtilStuff.executeQuery(colQuery);
        try{
            while(rs.next()){
                ano = rs.getString("ano");

                if(tempo.equals("Ano"))
                    colunas.addCol(ano);
                else{
                    cmp = rs.getString(tempo);
                    colunas.addCol(cmp, ano);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return colunas;
    }

    //retorna os resultados da análise em uma DefaultTableModel, exibida pela JTable
    public DefaultTableModel getModelProdutoTempo(String tempo, String dataInicio, String dataFinal) throws SQLException{
        ResultSet rs = getProdutoTempo(tempo,dataInicio, dataFinal);
        Vector row = new Vector<String>();
        Colunas colunas = getColumn(tempo);
        DefaultTableModel model =  new DefaultTableModel(colunas.getFormatedCol(), 0){
            @Override
              public boolean isCellEditable(int row, int column) {
                //all cells false
              return false;
            }};
        

        String[] rowArray = new String[colunas.size()];
        String current = "";
        String last = "";
        rs.first();
        last = rs.getString(1);
        rowArray[0] = last;
        if(tempo.equals("Ano"))
            rowArray[colunas.getColIndex(rs.getString("ano"))] = rs.getString("total");
        else 
            rowArray[colunas.getColIndex(rs.getString(tempo), rs.getString("ano"))] = rs.getString("total");

        while(rs.next()){
            current =rs.getString(1);
            if(current.equals(last)){
                 if (tempo.equals("Ano"))
                    rowArray [colunas.getColIndex(rs.getString("ano"))] = rs.getString("total");
                 else
                    rowArray[colunas.getColIndex(rs.getString(tempo),rs.getString("ano"))] =rs.getString("total");
            }
            else{
                model.addRow(rowArray);
                rowArray = new String[colunas.size()];
                rowArray[0] =current;
                 if (tempo.equals("Ano"))
                    rowArray [colunas.getColIndex(rs.getString("ano"))] = rs.getString("total");
                else
                    rowArray[colunas.getColIndex(rs.getString(tempo),rs.getString("ano"))] =rs.getString("total");
            }
            last = new String(current);
        }
        model.addRow(rowArray);

        return model;
    }
 
    //classe auxiliar utilizada na organização dos resultados
    class Colunas{
        HashMap <String, Integer> lolesIndex; 
        ArrayList <String> formatedCol;
        String unidade;
        int size;
        Colunas(Integer numberCols,String unidade, String regiao){
            this.unidade = unidade;
            formatedCol = new ArrayList<String>();
            lolesIndex = new LinkedHashMap<String,Integer>();
            lolesIndex.put(regiao, 0);
            formatedCol.add(regiao);
            size =1;
        }
        
        void addCol(String ano){
            this.formatedCol.add(ano.toString());
            size++;
        }
        
        void addCol(String tempo, String ano){
            char sigla = unidade.toCharArray()[0];
            this.formatedCol.add(tempo+" "+sigla+" de "+ ano.toString());
            this.lolesIndex.put(tempo+ano, size);
            size++;
        }
        String[] getFormatedCol(){
            String[] cols = new String[size];
            formatedCol.toArray(cols);
            return cols;        
        }
        
        int getColIndex (String ano){
            return this.formatedCol.indexOf(ano);
        }
        
        int getColIndex(String tempo,String ano){
           return lolesIndex.get(tempo+ano);
        }
        
        int size(){
            return size;
        }
        
    }
}