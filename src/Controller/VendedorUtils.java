/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ComboItem;
import javax.swing.JComboBox;
import Model.Connector;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author moacirnq
 */
public class VendedorUtils {
    /** Retorna um ComboBoxModel com os nomes e codigos das transportadoras **/
    public static DefaultComboBoxModel<ComboItem> nameComboModel() {
        DefaultComboBoxModel<ComboItem> cmb =  UtilStuff.comboModel("select codigo, nome "
                    + " from view_nomevendedor order by nome");
        cmb.addElement(new ComboItem("Internet","0"));
        return cmb;
    }
    
    public static int getItemIndex(int codigo, JComboBox cmb) {
        String text;
        String value="0";
        ComboItem aux;
        Connector con = new Connector();
        try {
            con.connect();
            con.query("select codigo, nome "
                    + "from view_nomevendedor where codigo=" + codigo);
            while (con.result().next()) {
                text = con.result().getString(2);
                value = con.result().getString(1);
                break;
            }
            
            for (int i =0 ; i < cmb.getModel().getSize(); i++) {
                aux = (ComboItem) cmb.getModel().getElementAt(i);
                if (aux.getValue() == value)
                    return i;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
