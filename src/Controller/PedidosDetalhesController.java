/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;
import java.sql.*;

/**
 *
 * @author koiti
 */
public class PedidosDetalhesController {
    private String dataPedido;
    private String dataEnvio;
    private String dataRecebe;
    private String enderecoFatura;
    private String enderecoEntrega;
    private String total;
    private String transportadora;
    private String frete;
    private String codConfirmacao;
    private String imposto;
   
    public ResultSetTable getPedidoDetalhes(String codPedido){ 
       
       ResultSetTable rs;
       String query_dtp = "select p.codigo as codigoproduto,p.nome,dtp.quantidade,"
               + "dtp.precounitario,dtp.desconto, (dtp.quantidade * dtp.precounitario)*(1-dtp.desconto) as total "
               + "from detalhespedido dtp, produto p where p.codigo = dtp.codigoproduto and codigopedido = "+ codPedido;
      rs = new ResultSetTable(query_dtp);
      return rs;
   }

   public void getPedidoInfo(String codPedido){
       String query = "select * from pedido where codigo = "+codPedido;
       String frete = new String();
       String total = new String();
       ResultSet rs = UtilStuff.executeQuery(query);
       try{
            while (rs.next()){
                dataPedido = UtilStuff.FormatData(rs.getString("dtpedido"));
                dataEnvio = UtilStuff.FormatData(rs.getString("dtenvio"));
                dataRecebe = UtilStuff.FormatData(rs.getString("dtRecebimento"));
                transportadora = getTransportadoraName(rs.getString("codigotransportadora"));
                enderecoEntrega = UtilStuff.getEndereco(rs.getString("enderecoentrega"));
                enderecoFatura = UtilStuff.getEndereco(rs.getString("enderecoFatura"));
                codConfirmacao = rs.getString("codigoconfirmacao");
                imposto = rs.getString("imposto");
                getValores(codPedido,total,frete);
                
            }
            
       }catch(SQLException ex){
           ex.printStackTrace();
       }   
   }
   
   private void getValores(String codPedido,String total, String frete){
       String query = "select frete,total from view_valores_pedidos where codigo = '"+codPedido+"'"; 
       ResultSet rs = UtilStuff.executeQuery(query);
       try{
           rs.next();
           this.total = rs.getString("total");
           this.frete = rs.getString("frete");
       }catch(SQLException ex){
           ex.printStackTrace();
       }
   }
   
   private String getTransportadoraName (String transp){
       String query = "select nome from transportadora where codigo = "+ transp;
       ResultSet rs = UtilStuff.executeQuery(query);
       String nome ="";
       try{
            while(rs.next()){
                nome = rs.getString("nome");
            }
       }catch (SQLException ex){
           ex.printStackTrace();
       }
       return nome;
   }
   
   public String getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(String dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public String getDataRecebe() {
        return dataRecebe;
    }

    public void setDataRecebe(String dataRecebe) {
        this.dataRecebe = dataRecebe;
    }

    public String getEnderecoFatura() {
        return enderecoFatura;
    }

    public void setEnderecoFatura(String EnderecoFatura) {
        this.enderecoFatura = EnderecoFatura;
    }

    public String getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(String EnderecoEntrega) {
        this.enderecoEntrega = EnderecoEntrega;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(String transportadora) {
        this.transportadora = transportadora;
    }

    public String getFrete() {
        return frete;
    }

    public void setFrete(String frete) {
        this.frete = frete;
    }
    
    public String getCodConfirmacao() {
        return codConfirmacao;
    }

    public void setCodConfirmacao(String codConfirmacao) {
        this.codConfirmacao = codConfirmacao;
    }
    
    public String getImposto() {
        return imposto;
    }

    public void setImposto(String imposto) {
        this.imposto = imposto;
    }

}
