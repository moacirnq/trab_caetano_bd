/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.newPDF;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author koiti
 */
    
    
public class DatasmartRegiaoTempoController {
    String query;
    //static final String[] REGIAO = {"cidade","estado","pais","continente"};
    //static final String[] TEMPO = {"mes","ano","trimestre","semestre"};
    /* regiao           tempo
       0 - cidade       0 - mes
       1 - estado       1 - ano
       2 - pais         2 - trimestre
       3 - continente   3 - semestre */
    public ResultSet pesquisar(String regiao, String tempo, String dataInicio, String dataFinal){
        int i;
        ResultSet rs;
        String cols = " reg."+regiao+" , tmp."+tempo+" ";
        
        if (tempo.equals("Ano"))
            query = "select "+ cols + " , sum(v.quantidade * v.precounitario) as total ";
        else
            query = "select "+ cols + " ,tmp.ano, sum(v.quantidade * v.precounitario) as total ";
        query += "from ftVenda v inner join dmRegiao reg on (reg.codigo = v.codigoregiao) inner join dmTempo tmp on (tmp.codigo = v.codigotempo) "+
                 "where tmp.data >= to_date('"+dataInicio+"','DD/MM/YYYY') and tmp.data <= to_date('"+dataFinal+"', 'DD/MM/YYYY')";
        if (tempo.equals("Ano"))
            query += "group by ("+ cols +") order by reg."+regiao+", ano";
        else
            query+= "group by ("+ cols +" ,tmp.ano) order by reg."+regiao+", ano, "+tempo;
   //     System.out.println(query);
        rs = UtilStuff.executeQuery(query);
        return rs;
    }
   
    
    
    public Colunas getColumn(String regiao, String tempo){
        Colunas colunas;
        String colQuery;
        String name="";
        String ano = "";
        if (tempo.equals("Ano")){
            colQuery = "select distinct ano from ( "+query+" ) order by ano";
            colunas = new Colunas(1,tempo,regiao);
        }
        else{
            colQuery = "select distinct "+tempo +", ano from ( "+ query + ") order by ano, "+tempo;
            colunas = new Colunas(2,tempo,regiao);
        }
        ResultSet rs = UtilStuff.executeQuery(colQuery);
        try {
            while(rs.next()){
                ano = rs.getString("ano");
                if (tempo.equals("Ano")){
                    colunas.addCol(ano);
                }
                else{
                    name = rs.getString(tempo);
                    colunas.addCol(name,ano);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return colunas;
    }
   
    public DefaultTableModel getModelRegiaoTempo(String regiao, String tempo, String dataInicio, String dataFinal) throws SQLException{
        ResultSet rs = pesquisar(regiao,tempo,dataInicio,dataFinal);   
        Vector row = new Vector<String>();
        Colunas colunas = getColumn (regiao,tempo);
        DefaultTableModel myModel = new DefaultTableModel(colunas.getFormatedCol(),0){
             @Override
              public boolean isCellEditable(int row, int column) {
                //all cells false
              return false;
              }};
        
       
        String[] rowArray = new String[colunas.size()];
        
        
        String current =""; 
        String last ="";
        rs.first();
        last = rs.getString(1);
        rowArray[0]=last;
        if (tempo.equals("Ano"))
            rowArray [colunas.getColIndex(rs.getString("ano"))] = rs.getString("total");
        else
            rowArray [colunas.getColIndex(rs.getString(tempo),rs.getString("ano"))] = rs.getString("total");
        while (rs.next()){
            current =rs.getString(1);
            if(current.equals(last)){
                 if (tempo.equals("Ano"))
                    rowArray [colunas.getColIndex(rs.getString("ano"))] = rs.getString("total");
                 else
                    rowArray[colunas.getColIndex(rs.getString(tempo),rs.getString("ano"))] =rs.getString("total");
            }
            else{
                //System.out.println(row);
                myModel.addRow(rowArray);
                rowArray = new String[colunas.size()];
                rowArray[0] =current;
                 if (tempo.equals("Ano"))
                    rowArray [colunas.getColIndex(rs.getString("ano"))] = rs.getString("total");
                else
                    rowArray[colunas.getColIndex(rs.getString(tempo),rs.getString("ano"))] =rs.getString("total");
            }
            last = new String(current);
        }
        myModel.addRow(rowArray);
            
        
        return myModel;        
    }
    
    
    class Colunas{
        HashMap <String, Integer> lolesIndex; 
        ArrayList <String> formatedCol;
        String unidade;
        int size;
        Colunas(Integer numberCols,String unidade, String regiao){
            this.unidade = unidade;
            formatedCol = new ArrayList<String>();
            lolesIndex = new LinkedHashMap<String,Integer>();
            lolesIndex.put(regiao, 0);
            formatedCol.add(regiao);
            size =1;
        }
        
        void addCol(String ano){
            this.formatedCol.add(ano.toString());
            size++;
        }
        
        void addCol(String tempo, String ano){
            char sigla = unidade.toCharArray()[0];
            this.formatedCol.add(tempo+" "+sigla+" de "+ ano.toString());
            this.lolesIndex.put(tempo+ano, size);
            size++;
        }
        String[] getFormatedCol(){
            String[] cols = new String[size];
            formatedCol.toArray(cols);
            return cols;        
        }
        
        int getColIndex (String ano){
            return this.formatedCol.indexOf(ano);
        }
        
        int getColIndex(String tempo,String ano){
           return lolesIndex.get(tempo+ano);
        }
        
        int size(){
            return size;
        }
        
    }
}
