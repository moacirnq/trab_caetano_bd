/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ComboItem;
import Model.Connector;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author moacirnq
 */
public class ProdutoUtils {
    /** Retorna um DefaultComboBoxModel<ComboItem> com os items da query. **/
    public static DefaultComboBoxModel<ComboItem> comboModel() {
        int numberOfRows = 0;
        Connector con;
        DefaultComboBoxModel<ComboItem> model;
        Vector<ComboItem> results;
        ComboItem aux;
        
        try {
            con = new Connector();
            con.connect();
            con.query("select codigo, nome, preco from produto order by nome");
            con.result().last();
            numberOfRows = con.result().getRow();
            con.result().first();
            
            results = new Vector<ComboItem>(numberOfRows);
            do {
                String nome = con.result().getString("nome");
                String codigo = con.result().getString("codigo");
                String preco = con.result().getString("preco");
                aux = new ComboItem( nome, codigo, Float.parseFloat(preco));
                results.add(aux);
            } while (con.result().next());
            model = new DefaultComboBoxModel<ComboItem>(results);
        }
        catch (SQLException e) {
            e.printStackTrace();
            model = null;
        }
        return model;
    }
}
