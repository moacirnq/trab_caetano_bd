/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author koiti
 */
public class DataValidator {
    public static boolean isThisDateValid(String dateToValidate, String dateFormat){		
		if(dateToValidate == null){
			return false;
		}
                if (dateToValidate.length()< dateFormat.length()) return false;
                
		
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		
		try {
			
			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);	
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
    
    public static String YY2YYYY(String date){
        if (date==null) return "";
        SimpleDateFormat yy = new SimpleDateFormat("dd/mm/yy");
        SimpleDateFormat yyyy =new SimpleDateFormat("dd/mm/yyyy");
        Date yyDate;
        try{
            yyDate = yy.parse(date);
        }catch (ParseException e){
            return "";
        }
        return yyyy.format(yyDate);
    }
    
    public static String YYYY2YY(String date){
        if (date==null) return "";
        SimpleDateFormat yy = new SimpleDateFormat("dd/mm/yy");
        SimpleDateFormat yyyy =new SimpleDateFormat("dd/mm/yyyy");
        Date yyyyDate;
        try{
            yyyyDate = yyyy.parse(date);
        }catch (ParseException e){
            return "";
        }
        return yy.format(yyyyDate);
    }

    public static Boolean isOrderCorrect(String data1, String data2){
        SimpleDateFormat sdt = new SimpleDateFormat("dd/mm/yyyy"); 
        Date d1,d2;
        try{
            d1 = sdt.parse(data1);
            d2 = sdt.parse(data2);
        }catch(ParseException ex){
           return false; 
        }
        if (d1.before(d2)||d1.equals(d2)) 
            return true;
        else
            return false;
    }
}
