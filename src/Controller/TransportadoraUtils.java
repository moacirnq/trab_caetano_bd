/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ComboItem;
import Model.Connector;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author moacirnq
 */
public class TransportadoraUtils {
    public static int getItemNameFromCode(int codigo, JComboBox cmb) {
        String text;
        String value = "";
        ComboItem aux;
        Connector con = new Connector();
        try {
            con.connect();
            con.query("select codigo, nome "
                    + "from transportadora where codigo = " + codigo);
            while (con.result().next()) {
                text = con.result().getString(2);
                value = con.result().getString(1);
                break;
            }
            for (int i =0 ; i < cmb.getModel().getSize(); i++) {
                aux = (ComboItem) cmb.getModel().getElementAt(i);
                if (aux.getValue() == value)
                    return i;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    /** Retorna um ComboBoxModel com os nomes e codigos das transportadoras **/
    public static DefaultComboBoxModel<ComboItem> nameComboModel() {
        return UtilStuff.comboModel("select codigo, nome "
                    + "from transportadora order by nome");
    }
}
